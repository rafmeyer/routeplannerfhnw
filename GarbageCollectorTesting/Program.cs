﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GarbageCollectorTesting
{
    public class Garbage
    {
        public Garbage()
        {
            String[] strings = new String[20000];
            for (int i = 0; i < 20000; ++i)
            {
                strings[i] = i.ToString();
            }
        }
        public static void CreateGarbage()
        {
            for(int i = 0; i < 1024; ++i)
            {
                Garbage current = new Garbage();
            }
        }
    }
    class Program
    {
        /*
         * Results:
         * Default (no config options): Avg = 2590ms
         * gcConcurrent = true & gcServer = true : Avg = 2481ms
         * gcConcurrent = true & gcServer = false : Avg = 2574ms
         * gcConcurrent = false & gcServer = true : Avg = 2483ms
         * gcConcurrent = false & gcServer = false : Avg = 2548ms
         */
        static void Main(string[] args)
        {
            var sw = new Stopwatch();


            long avg = 0;
            for (int j = 0; j < 3; ++j )
            {
                sw.Start();
                Garbage.CreateGarbage();
                sw.Stop();
                Console.WriteLine("Instance {0}: {1}", j, sw.ElapsedMilliseconds);
                avg += sw.ElapsedMilliseconds;
                sw.Reset();
            }
            avg /= 3;
            Console.WriteLine("Average: {0}", avg);

            Console.ReadKey();
        }
    }
}
