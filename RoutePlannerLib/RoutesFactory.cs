﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    public class RoutesFactory 
    {
        static public IRoutes Create(Cities cities)
        {
            return Create(cities, Properties.Settings.Default.RouteAlgorithm);
        }
        static public IRoutes Create(Cities cities, string algorithmClassName)
        {
            var t = Type.GetType(algorithmClassName);
            if (t == null)
            {
                return null;
            }
            var o = Activator.CreateInstance(t, new Object[] { cities });
            if (o == null)
            {
                return null;
            }
            return (IRoutes)o;
        } 
    }
}
