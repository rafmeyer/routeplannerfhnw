﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util
{
    public class SimpleObjectWriter
    {
        private TextWriter writer;
        public SimpleObjectWriter(TextWriter w)
        {
            writer = w;
        }
        public void Next(object o)
        {
            writer.Write(SerializeObject(o));
        }
        public string SerializeObject(object o)
        {
            var t = o.GetType();
            string r = "Instance of ";
            r += t.FullName + "\r\n";
            foreach (PropertyInfo p in t.GetProperties())
            {
                if (p.Name == "Index")
                    continue;

                if (p.PropertyType == typeof(string))
                {
                    r += p.Name + "=\"" + p.GetValue(o).ToString() + "\"\r\n";
                }
                else if (p.PropertyType.IsValueType)
                {
                    if (p.PropertyType == typeof(Double))
                    {
                        r += p.Name + "=" + ((Double)p.GetValue(o)).ToString(CultureInfo.InvariantCulture) + "\r\n";
                    }
                    else
                    {
                        r += p.Name + "=" + p.GetValue(o).ToString() + "\r\n";
                    }
                    
                }
                else //Assume we have an object now, so perform recursive serialization
                { 
                    r += p.Name;
                    r += " is a nested object...\r\n";
                    r += SerializeObject(p.GetValue(o));
                }
            }
            r += "End of instance\r\n";
            return r;
        }
    }
}
