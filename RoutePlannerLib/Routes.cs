
using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util;
using System.Diagnostics;


namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    /// <summary>
    /// Manages a routes from a city to another city.
    /// </summary>
    public abstract class Routes: IRoutes
    {
        protected List<Link> routes = new List<Link>();
        protected Cities cities;

        public int Count
        {
            get { return routes.Count; }
        }

        public bool ExecuteParallel { get; set; }

        /// <summary>
        /// Initializes the Routes with the cities.
        /// </summary>
        /// <param name="cities"></param>
        public Routes(Cities cities)
        {
            this.cities = cities;
        }

        public abstract List<Link> FindShortestRouteBetween(string fromCity, string toCity, TransportModes mode);
    

        /*
         * Lab 4 Aufgabe 3
         */ 
        /// <summary>
        /// Reads a list of links from the given file.
        /// Reads only links where the cities exist.
        /// </summary>
        /// <param name="filename">name of links file</param>
        /// <returns>number of read route</returns>
        public int ReadRoutes(string filename)
        {
            TraceSource traceSource = new TraceSource("Routes");
            traceSource.TraceEvent(TraceEventType.Information, 1, "ReadRoutes started");
            using (TextReader reader = new StreamReader(filename))
            {
                IEnumerable<string[]> routesAsStrings = reader.GetSplittedLines('\t');

                routesAsStrings.Where(r => cities.FindCity(r[0]) != null && cities.FindCity(r[1]) != null)
                    .ToList().ForEach(r => routes.Add(new Link(cities.FindCity(r[0]), cities.FindCity(r[1])
                        , cities.FindCity(r[0]).Location.Distance(cities.FindCity(r[1]).Location), TransportModes.Rail)));

                /*
                foreach (var a in routesAsStrings)
                {
                    City city1 = cities.FindCity(a[0]);
                    City city2 = cities.FindCity(a[1]);

                    // only add links, where the cities are found 
                    if ((city1 != null) && (city2 != null))
                    {
                        routes.Add(new Link(city1, city2, city1.Location.Distance(city2.Location),
                                                   TransportModes.Rail));
                    }
                }
                */
            }

            traceSource.TraceEvent(TraceEventType.Information, 2, "ReadRoutes ended");
            traceSource.Flush();
            traceSource.Close();
            return Count;
        }

        /*
         * Lab 4 Aufgabe 1.b 
         */ 
        public List<City> FindCitiesBetween(string fromCity, string toCity)
        {
            City from = cities.FindCity(fromCity);
            City to = cities.FindCity(toCity);

            return cities.FindCitiesBetween(from, to);
        }

        /*
         * Lab 4 Aufgabe 1.b 
         */ 
        public List<Link> FindPath(List<City> myCities, TransportModes mode)
        {
            List<Link> links = new List<Link>(myCities.Count - 1);
            City from, to;
            double distance;

            for(int i = 0; i < (myCities.Count - 1); i++) {
                from = myCities[i];
                to = myCities[i+1];

                distance = from.Location.Distance(to.Location);

                links.Add(new Link(from, to, distance, mode));
            }
            
            return links;
        }

        /*
         * Lab 4 Aufgabe 1.b 
         */
        public Link FindLink(City from, City to, TransportModes mode)
        {
            return new Link(from, to, from.Location.Distance(to.Location), mode);
        }

        /*
         * Lab 4 Aufgabe 1.b 
         */ 

        
    }
}
