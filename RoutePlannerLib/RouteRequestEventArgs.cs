﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    /*
     * Lab 3 Aufgabe 2.a
     */
    public class RouteRequestEventArgs : EventArgs
    {
        public string FromCity { get; private set; }
        public string ToCity { get; private set; }
        public TransportModes Mode { get; private set; }
        public RouteRequestEventArgs(string fromCity, string toCity, TransportModes mode)
        {
            FromCity = fromCity;
            ToCity = toCity;
            Mode = mode;
        }
    }
}
