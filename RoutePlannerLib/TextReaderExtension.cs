﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util
{
    /*
     * Lab 4 Aufgabe 3
     */ 
    static class TextReaderExtension
    {
        public static IEnumerable<string[]> GetSplittedLines(this TextReader reader, char splitter)
        {
            String tmpLine;

            while ((tmpLine = reader.ReadLine()) != null)
            {
                yield return tmpLine.Split(splitter);
            }
        } 
    }
}
