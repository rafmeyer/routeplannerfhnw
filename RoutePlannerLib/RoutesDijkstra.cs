﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    public class RoutesDijkstra: Routes
    {

        /*
         * Lab 3 Aufgabe 2.a
         */
        public delegate void RouteRequestHandler(object sender, RouteRequestEventArgs e);
        public event RouteRequestHandler RouteRequestEvent;


        public RoutesDijkstra(Cities cities) :
            base(cities)
        {
            RouteRequestEvent = (s, a) => { };
        }

        /*
        * Lab 11 Aufgabe 1
        */
        public async Task<List<Link>> FindShortestRouteBetweenAsync(string fromCity, string toCity, TransportModes mode)
        {
            return await Task.Run(() => FindShortestRouteBetween(fromCity, toCity, mode, null));
        }

        public async Task<List<Link>> FindShortestRouteBetweenAsync(string fromCity, string toCity, TransportModes mode, IProgress<string> reportProgress)
        {
            return await Task.Run(() => FindShortestRouteBetween(fromCity, toCity, mode, reportProgress));
        }

        public override List<Link> FindShortestRouteBetween(string fromCity, string toCity, TransportModes mode)
        {
            return FindShortestRouteBetween(fromCity, toCity, mode, null);
        }

        public List<Link> FindShortestRouteBetween(string fromCity, string toCity, TransportModes mode, IProgress<string> reportProgress)
        {
            Boolean reporting = reportProgress != null;
            /*
             * Lab 3 Aufgabe 2.b
             */
            if (reporting) reportProgress.Report("check RouteRequestEvent for null : done");
            if (RouteRequestEvent != null)
            {
                RouteRequestEvent(this, new RouteRequestEventArgs(fromCity, toCity, mode));
                if (reporting) reportProgress.Report("create new RouteRequestEvent : done");
            }

            var citiesBetween = FindCitiesBetween(fromCity, toCity);
            if (reporting) reportProgress.Report("FindCitiesBetween called : done");


            if (citiesBetween == null || citiesBetween.Count < 1 || routes == null || routes.Count < 1)
                return null;

            var source = citiesBetween[0];
            var target = citiesBetween[citiesBetween.Count - 1];

            Dictionary<City, double> dist;
            Dictionary<City, City> previous;

            // create emtpy List of city nodes for all citiesBetween
            var q = FillListOfNodes(citiesBetween, out dist, out previous);
            if (reporting) reportProgress.Report("FillListOfNodes called : done");
            // distance to startlocation is 0.0
            dist[source] = 0.0;

            // the actual algorithm
            previous = SearchShortestPath(mode, q, dist, previous);
            if (reporting) reportProgress.Report("SearchShortestPath called : done");

            // create a list with all cities on the route
            var citiesOnRoute = GetCitiesOnRoute(source, target, previous);
            if (reporting) reportProgress.Report("GetCitiesOnRoute called : done");

            // prepare final list if links
            if (reporting) reportProgress.Report("FindPath called : done");
            return FindPath(citiesOnRoute, mode);
        }

        private static List<City> FillListOfNodes(List<City> cities, out Dictionary<City, double> dist, out Dictionary<City, City> previous)
        {
            var q = new List<City>(); // the set of all nodes (cities) in Graph ;
            dist = new Dictionary<City, double>();
            previous = new Dictionary<City, City>();
            foreach (var v in cities)
            {
                dist[v] = double.MaxValue;
                previous[v] = null;
                q.Add(v);
            }

            return q;
        }

        /// <summary>
        /// Searches the shortest path for cities and the given links
        /// </summary>
        /// <param name="mode">transportation mode</param>
        /// <param name="q"></param>
        /// <param name="dist"></param>
        /// <param name="previous"></param>
        /// <returns></returns>
        private Dictionary<City, City> SearchShortestPath(TransportModes mode, List<City> q, Dictionary<City, double> dist, Dictionary<City, City> previous)
        {
            while (q.Count > 0)
            {
                City u = null;
                var minDist = double.MaxValue;
                // find city u with smallest dist
                foreach (var c in q)
                    if (dist[c] < minDist)
                    {
                        u = c;
                        minDist = dist[c];
                    }

                if (u != null)
                {
                    q.Remove(u);
                    foreach (var n in FindNeighbours(u, mode))
                    {
                        var l = FindLink(u, n, mode);
                        var d = dist[u];
                        if (l != null)
                            d += l.Distance;
                        else
                            d += double.MaxValue;

                        if (dist.ContainsKey(n) && d < dist[n])
                        {
                            dist[n] = d;
                            previous[n] = u;
                        }
                    }
                }
                else
                    break;

            }

            return previous;
        }

        /// <summary>
        /// Finds all neighbor cities of a city. 
        /// </summary>
        /// <param name="city">source city</param>
        /// <param name="mode">transportation mode</param>
        /// <returns>list of neighbor cities</returns>
        private List<City> FindNeighbours(City city, TransportModes mode)
        {
            var neighbors = new List<City>();
            foreach (var r in routes)
                if (mode.Equals(r.TransportMode))
                {
                    if (city.Equals(r.FromCity))
                        neighbors.Add(r.ToCity);
                    else if (city.Equals(r.ToCity))
                        neighbors.Add(r.FromCity);
                }

            return neighbors;
        }

        private List<City> GetCitiesOnRoute(City source, City target, Dictionary<City, City> previous)
        {
            var citiesOnRoute = new List<City>();
            var cr = target;

            while (previous[cr] != null)
            {
                citiesOnRoute.Add(cr);
                cr = previous[cr];
            }
            citiesOnRoute.Add(source);

            citiesOnRoute.Reverse();
            return citiesOnRoute;
        }


        /*
         * Lab 6 Aufgabe 3
         */
        public City[] FindCities(TransportModes transportMode)
        {
            if (routes != null)
            {
                return routes.Where(r => r.TransportMode.Equals(transportMode)).SelectMany(r => new[] { r.ToCity, r.FromCity }).Distinct().ToArray();
            }
            else
            {
                return null;
            }
        }
    }
}
