﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    public class SimpleObjectReader
    {
        private TextReader reader;
        public SimpleObjectReader(TextReader r)
        {
            reader = r;
        }

        public object Next()
        {
            var l = reader.ReadLine();
            if (l.StartsWith("Instance of "))
            {
                string t = l.Substring("Instance of ".Length);
                object o = Activator.CreateInstance(Type.GetType(t));
                while ((l = reader.ReadLine()) != null)
                {
                    if (l.EndsWith(" is a nested object..."))
                    {
                        o.GetType().GetProperty(l.Split(new char[] { ' ' })[0]).SetValue(o, Next());
                    }
                    else if (l.StartsWith("End of instance"))
                    {
                        return o;
                    }
                    else
                    {
                        var vr = l.Split(new char[] { '=' });
                        if (o.GetType().GetProperty(vr[0]).PropertyType == typeof(int)) 
                        {
                            o.GetType().GetProperty(vr[0]).SetValue(o, Convert.ToInt32(vr[1]));
                        }
                        else if (o.GetType().GetProperty(vr[0]).PropertyType == typeof(double))
                        {
                            o.GetType().GetProperty(vr[0]).SetValue(o, Convert.ToDouble(vr[1], CultureInfo.InvariantCulture));
                        }
                        else
                        {
                            o.GetType().GetProperty(vr[0]).SetValue(o, vr[1].Substring(1, vr[1].Length - 2));
                        }
                    }
                }
            }
            return null;
        }

    }
}
