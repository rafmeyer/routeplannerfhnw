﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util;
using System.Diagnostics;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    /*
     * Lab 2 Aufgabe 2.b
     */ 
    public class Cities
    {
        private List<City> cities = new List<City>();

        public int Count
        {
            get
            {
                return cities.Count;
            }
        }

        /*
         * Lab 4 Aufgabe 3 / Lab 6 Aufgabe 2
         */
        public int ReadCities(String filename)
        {
            TraceSource traceSource = new TraceSource("Cities");
            traceSource.TraceEvent(TraceEventType.Information, 1, "ReadCities started");
            int prevCount = Count;
            try 
            { 
                using (TextReader reader = new StreamReader(filename))
                {

                    cities.AddRange(reader.GetSplittedLines('\t').Select(c => new City(c[0], c[1], Convert.ToInt32(c[2]),
                        double.Parse(c[3], System.Globalization.CultureInfo.InvariantCulture),
                        double.Parse(c[4], System.Globalization.CultureInfo.InvariantCulture))));
                    /*reader.GetSplittedLines('\t').ToList().ForEach(c => cities.Add(new City(c[0], c[1], Convert.ToInt32(c[2]),
                        double.Parse(c[3], System.Globalization.CultureInfo.InvariantCulture),
                        double.Parse(c[4], System.Globalization.CultureInfo.InvariantCulture))));     */  
                }
            }
            catch(FileNotFoundException e)
            {
                traceSource.TraceEvent(TraceEventType.Critical, 3, "File '{0}' not found. StackTrace:\n{1}", filename, e.StackTrace);
            }
            traceSource.TraceEvent(TraceEventType.Information, 2, "ReadCities ended");
            traceSource.Flush();
            traceSource.Close();
            return Count - prevCount;
        }

        /*
         * Lab 2 Aufgabe 2.b 
         * 
         * ***************
         * ***KORREKTUR***
         * ***************
 
        public int ReadCities(String filename)
        {
            int prevCount = Count;

            using (TextReader reader = new StreamReader(filename))
            {
                String tmpLine;

                while ((tmpLine = reader.ReadLine()) != null)
                {
                    var tmpCity = tmpLine.Split('\t');

                    // no better way found, Convert.ToDouble() end in an exception
                    double latitude = double.Parse(tmpCity[3], System.Globalization.CultureInfo.InvariantCulture);
                    double longitude = double.Parse(tmpCity[4], System.Globalization.CultureInfo.InvariantCulture);

                    City city = new City(tmpCity[0], tmpCity[1], Convert.ToInt32(tmpCity[2]), latitude, longitude);

                    if (city != null)
                    {
                        cities.Add(city);
                    }
                }
            }

            return Count - prevCount;
        }
        */

        /*
         * Lab 2 Aufgabe 2.c
         */
        public City this[int index]
        {
            get
            {
                if (index < 0 || index >= Count) return null;
                return this.cities[index];
            }
            set
            {
                if (index >= 0 || index < Count) this.cities[index] = value;
            }
        }

        /*
         * Lab 2 Aufgabe 2.d / Lab 6 Aufgabe 1
         */
        public List<City> FindNeighbours(WayPoint location, double distance)
        {
            return cities.Where(c => location.Distance(c.Location) <= distance).ToList();
        }

        /*
         * Lab 3 Aufgabe 1
         */ 
        public City FindCity(string cityName)
        {
            try
            {
                return cities.Find((c) =>
                {
                    return c.Name.Equals(cityName, StringComparison.OrdinalIgnoreCase);
                });
            }
            catch (Exception)
            {
                return null;
            }
        }

        /*
         * Lab 4 Aufgabe 1.a
         */ 
        /// <summary>
        /// Find all cities between 2 cities 
        /// </summary>
        /// <param name="from">source city</param>
        /// <param name="to">target city</param>
        /// <returns>list of cities</returns>
        public List<City> FindCitiesBetween(City from, City to)
        {
            var foundCities = new List<City>();
            if (from == null || to == null)
                return foundCities;

            foundCities.Add(from);

            var minLat = Math.Min(from.Location.Latitude, to.Location.Latitude);
            var maxLat = Math.Max(from.Location.Latitude, to.Location.Latitude);
            var minLon = Math.Min(from.Location.Longitude, to.Location.Longitude);
            var maxLon = Math.Max(from.Location.Longitude, to.Location.Longitude);

            // rename the name of the "cities" variable to your name of the internal City-List
            foundCities.AddRange(cities.FindAll(c =>
                c.Location.Latitude > minLat && c.Location.Latitude < maxLat
                        && c.Location.Longitude > minLon && c.Location.Longitude < maxLon));

            foundCities.Add(to);
            InitIndexForAlgorithm(foundCities);
            return foundCities;
        }

        private List<City> InitIndexForAlgorithm(List<City> foundCities)
        {
            for(int index = 0; index < foundCities.Count; ++index)
            {
                foundCities[index].Index = index;
            }
            return foundCities;
        }
    }
}
