﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Office.Interop.Excel;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Export
{
    public class ExcelExchange
    {
        public void WriteToFile(String fileName, City from, City to, List<Link> links)
        {
            Microsoft.Office.Interop.Excel.Application app = 
                new Microsoft.Office.Interop.Excel.Application();

            if (app == null)
            {
                Console.WriteLine("Failed to initialize Excel");
                return;
            }

            var wb = app.Workbooks.Add();

            var ws = 
                (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets.get_Item(1);


            // Render titles
            ws.Cells[1, 1] = "From";
            ws.Cells[1, 2] = "To";
            ws.Cells[1, 3] = "Distance";
            ws.Cells[1, 4] = "Transport Mode";

            Microsoft.Office.Interop.Excel.Range r = 
                ws.get_Range("A1", "D1");

            r.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous,
                Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin);
            r.Font.Bold = true;
            r.Font.Size = 14;

            int i = 2;
            foreach(var l in links)
            {
                ws.Cells[i, 1] = l.FromCity.Name + " (" + l.FromCity.Country + ")"; ;
                ws.Cells[i, 2] = l.ToCity.Name + " (" + l.ToCity.Country + ")";
                ws.Cells[i, 3] = l.Distance;
                ws.Cells[i, 4] = l.TransportMode.ToString();
                ++i;
            }

            app.DisplayAlerts = false;
            ws.SaveAs(fileName, 
                Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, 
                Type.Missing, Type.Missing, true, false, 
                XlSaveAsAccessMode.xlNoChange, 
                XlSaveConflictResolution.xlLocalSessionChanges, 
                Type.Missing, Type.Missing);
            app.Quit();
        }

    }
}