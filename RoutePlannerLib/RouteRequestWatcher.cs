﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    /*
     * Lab 3 Aufgabe 2.c
     */ 
    public class RouteRequestWatcher
    {
        Dictionary<string, int> RequestedCities;

        public RouteRequestWatcher()
        {
            RequestedCities = new Dictionary<string, int>();
        }

        public void LogRouteRequests(object source, RouteRequestEventArgs args)
        {
            if (RequestedCities.ContainsKey(args.ToCity))
                RequestedCities[args.ToCity]++;
            else
                RequestedCities[args.ToCity] = 1;
        }
        public int GetCityRequests(string cityName)
        {
            try
            {
                return RequestedCities[cityName];
            }
            catch(KeyNotFoundException)
            {
                return 0;
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}
