﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Dynamic
{
    public class World: DynamicObject
    {
        Cities cities;

        public World(Cities c)
        {
            cities = c;
        }
        public override bool TryInvokeMember(InvokeMemberBinder binder,
                                     object[] args,
                                     out object result)
        {
            dynamic res;

            City c = cities.FindCity(binder.Name);
            if (c == null)
            {
                res = "The city \"" + binder.Name + "\" does not exist!";
            }
            else
            {
                res = c;
            }
            result = res;
            return true;
        }
    }
}
