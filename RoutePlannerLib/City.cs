﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    /*
     * Lab 2 Aufgabe 2.a
     */ 
    public class City 
    {
        [XmlIgnore]
        public int Index { get; set; }

        public string Name { get; set; }
        public string Country { get; set; }
        public int Population { get; set; }
        public WayPoint Location { get; set; }

        public City(String name, String country, int population, double latitude, double longitude)
        {
            Name = name;
            Country = country;
            Population = population;
            Location = new WayPoint(name, latitude, longitude);
        } 
        public City()
        {

        }

        public override string ToString()
        {
            return Name + " in the County " + Country + " has a population from " + Population + '\n' + " exact location is: " + Location;
        }

        /*
         * Lab 4 Aufgabe 1.c
         */
        #region Lab 4 Equals
        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to City return false.
            City c = obj as City;
            if ((Object)c == null)
            {
                return false;
            }
            
            // Return true if the fields match:
            return (this.Name == c.Name) && (this.Country == c.Country);
        }

        public bool Equals(City c)
        {
            // If parameter is null return false:
            if ((object)c == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.Name == c.Name) && (this.Country == c.Country);
        }

        public override int GetHashCode()
        {
            return Convert.ToInt32(this.Name.GetHashCode() ^ this.Country.GetHashCode());
        }
        #endregion
    }
}
