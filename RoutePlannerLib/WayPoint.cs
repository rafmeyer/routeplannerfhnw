﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    /*
     * Lab 1 Aufgabe 2
     */
    public class WayPoint
    {
        public string Name { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public WayPoint(string name, double latitude, double longitude)
        {
            Name = name;
            Latitude = latitude;
            Longitude = longitude;
        }

        public WayPoint()
        {

        }

        /*
         * Lab 2 Aufgabe 1.a
         */ 
        public override string ToString()
        {
            if (Name == null || Name == "")
                return string.Format("WayPoint: {0:N2}/{1:N2}", Latitude, Longitude);

            return string.Format("WayPoint: {0} {1:N2}/{2:N2}", Name, Latitude, Longitude);
        }

        /*
         * Lab 2 Aufgabe 1.b
         */ 
        public double Distance(WayPoint targert)
        {
            var r = 6371;

            var rad = Math.PI/180;

            var latitude1 = targert.Latitude * rad;
            var latitude2 = Latitude * rad;

            var longitude1 = targert.Longitude * rad;
            var longitude2 = Longitude * rad;

            return r*
                   Math.Acos(Math.Sin(latitude1)*Math.Sin(latitude2) +
                             Math.Cos(latitude1)*Math.Cos(latitude2)*Math.Cos(longitude2 - longitude1));
        }

        /*
         * Lab 4 Aufgabe 2 
         */
        #region Operatoren

        public static WayPoint operator +(WayPoint a, WayPoint b)
        {
            return new WayPoint(a.Name, a.Latitude + b.Latitude, a.Longitude + b.Longitude);
        }

        public static WayPoint operator -(WayPoint a, WayPoint b)
        {
            return new WayPoint(a.Name, a.Latitude - b.Latitude, a.Longitude - b.Longitude);
        }

        #endregion
    }
}
